package questao;

public class Fibonacci {

	public static void main(String[] args) {
		int[] vetor = obterSequenciaFibonacci(25, 5, 8);
		imprimir("Item A: ", vetor);
		imprimir("Item B: ", obterMediaAritmetica(vetor));
		imprimir("Item C: ", obterSomatorio(vetor));
		imprimir("Item D: ", obterQuantidadeDivQuatroTres(vetor));
		imprimir("Item E: ", obterSequenciaImpares(vetor));
		imprimir("Item F: ", obterSequenciaDecrescente(vetor));
		imprimir("Item G: ", obterSequenciaFinal(vetor,15,3));

	}

	public static int[] obterSequenciaFibonacci(int tam, int inicial1, int inicial2) {
		int[] vetor = new int[tam];
		for (int i = 0; i < vetor.length; i++) {
			if (i == 0) {
				vetor[i] = i + inicial1;
			}
			if (i == 1) {
				vetor[i] = i + (inicial2 - 1);
			} else if (i > 1) {
				vetor[i] = vetor[i - 1] + vetor[i - 2];
			}

		}
		return vetor;

	}

	public static double obterMediaAritmetica(int[] vetor) {
		double resultado = 0.0;
		int[] contadorPar = new int[20];
		int[] contadorImpar = new int[20];

		for (int i = 0, j = 0, c = 0; i < vetor.length; i++) {
			if (vetor[i] % 2 != 0) {
				contadorImpar[j] = vetor[i];
				j++;
			} else {
				contadorPar[c] = vetor[i];
				c++;
			}
		}
		double par = contadorPar[2];
		double impar = contadorImpar[8];

		resultado = (par + impar) / 2;

		return resultado;

	}

	public static int obterSomatorio(int[] vetor) {
		int resultado = 0;

		for (int i = 0; i < vetor.length; i++) {
			resultado += vetor[i];
		}

		return resultado;

	}

	public static int obterQuantidadeDivQuatroTres(int[] vetor) {
		int resultado = 0;
		for (int i = 0; i < vetor.length; i++) {
			if (vetor[i] % 4 == 0 && vetor[i] % 3 == 0) {
				resultado++;
			}
		}

		return resultado;

	}

	public static int[] obterSequenciaImpares(int[] vetor) {
		int contador = 0;
		for (int i = 0; i < vetor.length; i++) {
			if (vetor[i] % 2 == 0) {
				contador++;
			}
		}
		int[] valoresPares = new int[contador];

		for (int i = 0,j=0; i < vetor.length; i++) {
			if (vetor[i] % 2 == 0) {
				valoresPares[j] = vetor[i];
				j++;
			}
		}

		return valoresPares;
	}

	public static int[] obterSequenciaDecrescente(int[] vetor) {
		int vector [] = new int [vetor.length];	
		
		for (int i = 0; i < vector.length; i++) {
			for (int j = 0; j < vetor.length; j++) {
				if (i==0) {
					vector[i]=vetor[vetor.length-1];
				}else if (vetor[j]<vector[(i-1)]) {
					vector[i]=vetor[j];
				}
			}			
		}
		return vector;

	}
	
	public static int[] obterSequenciaFinal(int[] vetor, int valor, int razao) {
		int vector [] = new int [(vetor.length/2)];	
		
		for (int i = 0; i < vector.length; i++) {
			if (i==0) {
				vector[i]=valor*razao;
			}
			if(i>=1){
				vector[i]=vector[i-1]*razao;
			}
									
		}
		return vector;
		
	}

//	METODOS DE IMPRESS�O
	public static void imprimir(String txt, int[] vetor) {
		System.out.println("");
		System.out.print(txt);
		for (int i = 0; i < vetor.length; i++) {
			System.out.print(vetor[i] + " ");
		}

	}

	public static void imprimir(String txt, double vetor) {
		System.out.println("");
		System.out.print(txt + vetor);

	}

	public static void imprimir(String txt, int vetor) {
		System.out.println("");
		System.out.print(txt + vetor);

	}

}
