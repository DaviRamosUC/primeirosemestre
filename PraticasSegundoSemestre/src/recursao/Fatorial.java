package recursao;

public class Fatorial {

	public static void main(String[] args) {
		System.out.println(fatorial(5));

	}
	
	public static int fatorial(int vlr) {
		if(vlr < 2) 
			return 1;			
		return vlr * fatorial(vlr - 1);
	}

}
