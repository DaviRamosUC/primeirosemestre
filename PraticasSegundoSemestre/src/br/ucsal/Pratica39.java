package br.ucsal;

public class Pratica39 {

	/*
	 * Elabore uma solu��o para a partir de qualquer nome informado, seja obtido
	 * dinamicamente seu sobrenome e depois esse sobrenome seja impresso
	 */

	public static void executar() {
		String txt = "Fabricio Queiroz Bezerra da Silva";
		// Silva, Jos�
		imprimir(sobrenomeNome(txt));

	}

	public static String sobrenomeNome(String txt) {
		int espaco = 0;
		for (int i = 0; i < txt.length(); i++) {
			if (txt.substring(i, i + 1).equals(" ")) {
				espaco = i;
			}
		}
		return txt.substring(espaco + 1, txt.length());
	}

	public static void imprimir(String txt) {

		System.out.print(txt);

	}

	public static void main(String[] args) {

		executar();
	}

}
