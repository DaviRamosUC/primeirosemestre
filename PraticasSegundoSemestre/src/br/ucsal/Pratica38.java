package br.ucsal;

import java.util.Scanner;

public class Pratica38 {

	/*
	 * A escala Celsius possui o ponto zero na temperatura que a �gua congela e
	 * 100�C na temperatura que a �gua ferve, as medidas.
	 * 
	 * A escala Fahrenheit tem o congelamento da �gua em 32�F e a ebuli��o em 212�F.
	 * A escala Kelvin congela a �gua em 273 (K) e ferve a 373 K. Fonte:
	 * http://www.infoescola.com/fisica/conversao-de-escalas-termometricas/
	 * 
	 * A partir do texto acima, crie um ConversorDeTemperatura na qual cada m�todo
	 * dever� implementar o c�lculo necess�rio para convers�o espec�fica da
	 * temperatura.
	 * 
	 * As convers�es s�o:
	 * 
	 * (1) De Celsius para Kelvin; (2) De Kelvin para Celsius; (3)De Celsius para
	 * Fahrenheit; (4) De Fahrenheit para Celsius; (5) De Kelvin para Fahrenheit;
	 * (6) De Fahrenheit para Kelvin.
	 * 
	 * � importante que: 1. Cada convers�o seja feita em um m�todo especifico.
	 * 
	 * 2. A classe "main" dever� possuir apenas as instru��es necess�rias para
	 * solicitar os valores com as temperaturas e qual a convers�o que ser� feita.
	 * 
	 * 3. N�o dever� ocorrer nenhuma instru��o de impress�o (System.out.print ...)
	 * em nenhum m�todo exceto o m�todo "impress�o" criado especificamente para essa
	 * finalidade.
	 * 
	 * 4. Ao final, o algoritmo dever� apresentar o resultado para o usu�rio.
	 */

	public static void obterDados() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Informe o valor que deseja conveter");
		double vlr = sc.nextDouble();
		System.out.println(
				"Informe qual convers�o deseja fazer \n(1) De Celsius para Kelvin; \n(2) De Kelvin para Celsius; \n(3) De Celsius para Fahrenheit; \n(4) De Fahrenheit para Celsius; \n(5) De Kelvin para Fahrenheit; \n(6) De Fahrenheit para Kelvin.");
		int op = sc.nextInt();
		sc.close();
		execussao(op,vlr);

	}

	public static void main(String[] args) {
		obterDados();
	}

	public static Double kc(double vlr) {
		double kel = vlr + 273;
		return kel;

	}

	public static Double ck(double vlr) {
		double cel = 273 - vlr;
		return cel;
	}

	public static Double cf(double vlr) {
		double fah = (1.8 * vlr) + 32;
		return fah;
	}

	public static Double fc(double vlr) {
		double cel = (vlr - 32) / 1.8;
		return cel;
	}

	public static Double kf(double vlr) {
		return ((vlr - 273.00) * 9/5) + 32.00;
	}

	public static Double fk(double vlr) {
		return ((vlr - 32.00) * (5/9)) + 273.00;
	}

	public static void execussao(int op, double vlr) {
		double saida=0;

		switch (op) {
		case 1:
			saida = ck(vlr);
			break;
		case 2:
			saida = kc(vlr);
			break;
		case 3:
			saida = cf(vlr);
			break;
		case 4:
			saida = fc(vlr);
			break;
		case 5:
			saida = kf(vlr);
			break;
		case 6:
			saida = fk(vlr);
			break;

		default:
			System.out.println("N�o existe essa opera��o dentre as op��es fornecidas");
			break;
		}
		
		impressao("Resultado: "+ saida);
	}

	public static void impressao(String msg) {
		System.out.println("\n" + msg);
	}

}
