package br.ucsal;

public class Pratica52 {

	/*
	 * Elabore uma solu��o para preencher dinamicamente um array bidimensional de
	 * tamanho 3 x 5 com uma sequencia crescente de 15 valores inteiros, pares
	 * iniciados por 8. O array deve ser retornado em um m�todo espec�fico, cujo
	 * par�metros ser�o os valores que ir�o definir o seu tamanho
	 */
	public static void main(String[] args) {
		imprimir(obterMatriz(3, 5));
	}
	
	public static int [][] obterMatriz(int l, int c) {
		int [][] matriz = new int [l][c];
		
		for (int i = 0, z=8; i < matriz.length; i++) {
			for (int j = 0; j < matriz[i].length; j++,z+=2) {
				matriz[i][j]+=z;
			}
		}
		
		return matriz;
	}
	
	public static void imprimir(int [][] matriz) {
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz[i].length; j++) {
				if (matriz[i][j]<10) {
					System.out.print(" ");
				}
				System.out.print(matriz[i][j]+" ");
			}
			System.out.println();
		}
	}

}
