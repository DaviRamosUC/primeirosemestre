package br.ucsal;

public class Pratica54 {

	public static void main(String[] args) {
		imprimir(obterSequenciaFibonacci(5, 8));
		System.out.println();
		imprimir(qtdeZeros(360, 9));
		System.out.println();
		imprimir(obterSequenciaFibonacciPadrao(obterSequenciaFibonacci(5, 8)));

	}
	
	public static int[][] obterSequenciaFibonacci(int l, int c) {
		int[][] arr = new int[l][c];
		for (int a = 0, pri = 0, seg = 1, aux; a < arr.length; a++) {
			for (int b = 0; b < arr[a].length; b++) {
				arr[a][b] = pri;
				aux = pri + seg;
				pri = seg;
				seg = aux;
			}
		}
		return arr;
	}
	
	public static String qtdeZeros(int vlr, int padrao) {
		String zero = "";
		for (int i = 0; i < (padrao - (vlr + "").length()); i++) {
			zero += "0";
		}
		return zero + vlr;
	}
	
	public static String[][] obterSequenciaFibonacciPadrao(int[][] arr) {
		String[][] arrP = new String[5][8];
		
			for (int i = 0; i < arrP.length; i++) {
				for (int j = 0; j < arrP[i].length; j++) {
					arrP[i][j] = qtdeZeros(arr[i][j], 9);
				}
			}
		
		return arrP;
	}
//	-------------------------------------------------------------------------------------
	public static void imprimir(int [][] matriz) {
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz[i].length; j++) {
				if (matriz[i][j]<10) {
					System.out.print(" ");
				}
				System.out.print(matriz[i][j]+" ");
			}
			System.out.println();
		}
	}
	public static void imprimir(String [][] matriz) {
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz[i].length; j++) {
				System.out.print(matriz[i][j]+" ");
			}
			System.out.println();
		}
	}

	public static void imprimir(int x) {
		System.out.println(x);
	}
	public static void imprimir(String x) {
		System.out.println(x);
	}
 
	public static void imprimir(double x) {
		System.out.println(x);
	}

}
