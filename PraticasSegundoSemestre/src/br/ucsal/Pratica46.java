package br.ucsal;

import javax.swing.JOptionPane;

public class Pratica46 {

	/*
	 * Elabore uma solu��o para inserir em um array unidimensional o nome de 06
	 * alunos da turma e retornar o nome do aluno a partir do �ndice informado.
	 */
	
	
	public static void main(String[] args) {
		executor();
	}

	public static void executor() {
		String [] vetor = new String [6];
		inserirNomes(vetor);
		String nome = escolherNome();	
		
		Impressao.imprimir(obterIndiceElemento (nome, vetor));
	}
	
	public static void inserirNomes(String [] vetor){
		for (int i = 0; i < vetor.length; i++) {
			vetor[i] = JOptionPane.showInputDialog("Digite o nome que deseja inserir");
		}
	}
	
	public static String escolherNome() {
		String nome = JOptionPane.showInputDialog("Digite o nome que voc� deseja procurar");
		return nome;
	}
	
	public static int obterIndiceElemento (String str, String [] vetor) {
		for (int i = 0; i < vetor.length; i++) {
			if (vetor[i].equals(str)) {
				return i;
			}
		}
		return 0;
	}

}
