package br.ucsal;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;

public class Impressao {

	public static void main(String[] args) {
		

	}
	
	public static void imprimir (String txt) {
		System.out.println(txt);
	}
	
	public static void imprimir (int txt) {
		System.out.println(txt);
	}
	
	public static void imprimir (Date data) {
		System.out.println(data);
	}
	
	public static void imprimir (Calendar data) {
		System.out.println(data);
	}
	
	public static void imprimir (LocalDate localdate) {
		System.out.println(localdate);
	}
	
	public static void imprimir (double vlr) {
		System.out.println(vlr);
	}
	
	public static void imprimir (float vlr) {
		System.out.println(vlr);
	}
	
	public static void imprimir (int [] vlr) {
		for (int i = 0; i < vlr.length; i++) {
			System.out.print(vlr[i]+" ");
		}
	}

	public static void imprimir (String [] vlr) {
		for (int i = 0; i < vlr.length; i++) {
			System.out.print(vlr[i]+" ");
		}
	}
	

}
