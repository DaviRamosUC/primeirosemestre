package br.ucsal;

public class Pratica49 {

	/*
	 * Elabore uma solu��o para construir dinamicamente uma sequencia de valores
	 * inteiros, crescentes, pares, iniciada pelo valor 2, contendo 15 elementos.
	 * Considere que a sequencia dever� ser constru�da de forma que cada valor da
	 * sequencia seja um elemento de um array unidimensional que ser� retornado de
	 * um m�todo. Ao final o array dever� ser impresso apresentando a sequencia
	 * elaborada.
	 */
	public static void main(String[] args) {
		Impressao.imprimir(obterSequencia(15));

	}
	
	public static int[] obterSequencia(int tam){
		int [] seq = new int[tam];
		for (int i = 0, p= 2; i < seq.length; i++,p+=2) {
			seq[i]=p;
		}
		
		return seq;
	}

}
