package br.ucsal;

public class Pratica51 {

	/*Elabore uma solu��o para criar um array unidimensional contendo uma sequencia aritm�tica de 20 valores inteiros, 
	 * positivos iniciada com o valor 3, tendo a raz�o o valor 6.
	 * A partir dessa solu��o, apresente os resultados solicitados nos itens abaixo sendo que cada item deve ser solucionado 
	 * em um m�todo espec�fico. S�o:
	 * A) retornar o array constru�do dinamicamente com a sequencia solicitada.
	 * B) retornar a soma de todos os elementos da sequencia elaborada no item "a"
	 * C) retornar um array constru�do dinamicamente apenas com os elementos pares da mesma sequencia do item "a".
	 * D) retornar um array constru�do dinamicamente com os mesmos elementos do item "a" em ordem decrescente.
	 * E) retornar a m�dia aritm�tica do 2� e 15� elementos do array elaborado no item "a"
	 * G) retornar a m�dia ponderada do 5� e 11� elementos do array, considerando os pesos 7 e 3 respectivamente.
	Regras:
	 * 1) a impress�o de todos as informa��es solicitadas nos itens dever�o ser feitas utilizando m�todos espec�ficos com o nome "imprimir".
	 * 2)n�o pode ser utilizada nenhuma impress�o nos m�todos, exceto no m�todo principal
	 * 3) o System.out.print(ln) ser� usado exclusivamente nos m�todos de impress�o
	 * 4) o m�todo principal (main) ser� usado apenas para a execu��o dos m�todos e deve solicitar a impress�o dos resultados 
	atrav�s dos m�todos espec�ficos de impress�o */
		
	public static void inicializador() {

		int a1=3, r=6;
		final int [] Vetor = itemA(a1,r);
		imprimir("Quest�o A: ", Vetor);
		imprimir("Quest�o B: " + itemB(Vetor));
		imprimirTratado("Quest�o C: " , itemC(Vetor));
		imprimir("Quest�o D: ",itemD(Vetor));
		imprimir("Quest�o E: ",itemE(Vetor));
		imprimir("Quest�o G: ",itemG(Vetor));
	}
	
	// QUEST�ES
	public static int [] itemA (int a1, int r) {
		int vetor [] = new int [20];
		
		for (int i = 0; i < vetor.length; i++) {
			vetor[i]=a1 + i * r;
		}
		return vetor;
	}

	public static int itemB (int vetor[]) {
		int soma=0;
		
		for (int i = 0; i < vetor.length; i++) {
			soma= soma+vetor[i];
		}
		return soma;
	}

	public static int[] itemC (int vetor[]) {
		int vector [] = new int [vetor.length];
				
		for (int i = 0; i < vetor.length; i++) {
			if (vetor[i]%2==0) {
				vector[i]= vetor[i];	
			}
		}	
		return vector;
		
	}	

	public static int[] itemD (int vetor[]) {
		int vector [] = new int [vetor.length];	
		
		for (int i = 0; i < vector.length; i++) {
			for (int j = 0; j < vetor.length; j++) {
				if (i==0) {
					vector[i]=vetor[vetor.length-1];
				}else if (vetor[j]<vector[(i-1)]) {
					vector[i]=vetor[j];
				}
			}			
		}
		return vector;
		
	}	

	public static int itemE (int vetor[]) {
		int media= (vetor[1] + vetor[14])/2;		
		return media;
		
	}	
	public static double itemG (int vetor[]) {
		final double a=7.0, b=3.0;
		double media= ((vetor[4]*a) + (vetor[10]*b))/(a+b);		
		return media;
		
	}	
	
	
	//	METODOS DE IMPRESS�O
	public static void imprimir (String txt,int [] vlr) {
		System.out.print(txt);
		for (int i = 0; i < vlr.length; i++) {
			System.out.print(vlr[i]+" ");
		}
		System.out.println("");
	}
	public static void imprimir (String txt,int vlr) {
		System.out.print(txt);
		System.out.println(vlr);
	}
	public static void imprimir (String txt,double vlr) {
		System.out.print(txt);
		System.out.println(vlr);
	}
	
	public static void imprimirTratado (String txt,int [] vlr) {
		int contador=0;
		System.out.print(txt);
		for (int i = 0; i < vlr.length; i++) {
			if (vlr[i]==0) {
				contador++;
			}else {
			System.out.print(vlr[i]+" ");
			}
		}
		if (contador == vlr.length) {
			System.out.println("O Vetor n�o contem nenhum elemmento par");
		}
	}
	
	public static void imprimir (String txt) {
		System.out.println(txt);
	}
	
	public static void imprimir (int txt) {
		System.out.println(txt);
	}

	public static void imprimir (double txt) {
		System.out.println(txt);
	}

	// MAIN		
	public static void main(String[] args) {
		inicializador();
	}
}
