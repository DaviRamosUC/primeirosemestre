package br.ucsal;

import javax.swing.JOptionPane;

public class Pratica47 {

	/*
	 * Elabore uma solu��o para a inserir 13 alunos em um array unidimensional e 13
	 * matriculas em outro array unidimensional. Uma vez criados os arrays, deve-se
	 * obter o nome e a matricula do aluno a partir do �ndice informado. OBS:
	 * utilize m�todos diferentes para cada parte solucionada dessa quest�o.
	 */
	
	public static void executor() {
		int indice = obterIndice();
		Impressao.imprimir(obterNomeMatricula(indice, obterAlunos(), obterMatricula()));
	}
	
	public static int obterIndice() {
		int indice = 0;
		indice = Integer.parseInt(JOptionPane.showInputDialog("Informe um indice"));
		
		return indice;
	}
	
	public static String obterNomeMatricula(int indice, String [] vetor1, String [] vetor2) {
		if (indice != 0) {
			return "O nome do aluno �: "+vetor1[indice] + " e a matricula �: " + vetor2 [indice];
		}
		return null;
	}
	
	public static String[] obterAlunos() {
		String[] aluno = new String[13];
		aluno[0] = "Bruno Antas Costa Vieira";
		aluno[1] = "Davi Ramos Lima";
		aluno[2] = "Fernando Lira De Oliveira Damasceno";
		aluno[3] = "Jorge Lessa Ferreira Junior";
		aluno[4] = "Lucas Freire Mota";
		aluno[5] = "Lucca Barbosa Nygaard";
		aluno[6] = "Lucilandia Oliveira Babani";
		aluno[7] = "Luis Guilherme De Oliveira Carvalho";
		aluno[8] = "Nathalie Dos Santos Rosa";
		aluno[9] = "Raphael Silva Oliveira";
		aluno[10] = "Raul Rosa De Oliveira Pacheco";
		aluno[11] = "Rodrigo Fernandez Gomes Melo";
		aluno[12] = "Samila Duarte Silva";
		return aluno;
	}
	
	public static String[] obterMatricula() {
		String[] matricula = new String[13];
		matricula[0] = "1111111";
		matricula[1] = "2222222";
		matricula[2] = "3333333";
		matricula[3] = "4444444";
		matricula[4] = "5555555";
		matricula[5] = "6666666";
		matricula[6] = "7777777";
		matricula[7] = "8888888";
		matricula[8] = "9999999";
		matricula[9] = "1010101";
		matricula[10] = "1212121";
		matricula[11] = "9191919";
		matricula[12] = "3535353";
		return matricula;
	}
	
	public static void main(String[] args) {
		executor();
	}

}
