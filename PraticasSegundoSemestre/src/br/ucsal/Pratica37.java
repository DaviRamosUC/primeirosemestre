package br.ucsal;

import java.util.Scanner;

public class Pratica37 {

	public static void main(String[] args) {
		
		entradadevalores();		
		
	}
	
	public static void entradadevalores() {
		Scanner sc = new Scanner(System.in);
		int op;
		double vlr1=0, vlr2=0;
		
		System.out.println("Qual opera��o voc� deseja fazer| 1- Soma | 2-Subtra��o | 3- Multiplica��o | 4- Divis�o");
		op=sc.nextInt();
		System.out.println("Digite dois n�meros para a opera��o");
		vlr1 = sc.nextDouble();
		vlr2 = sc.nextDouble();
		resultado(op,vlr1,vlr2);
		
		sc.close();
	}
	
	public static Double soma(double parc1, double parc2) {
		double a= parc1 + parc2;
		return a;
	}
	
	public static Double subtracao(double parc1, double parc2) {
		double a= parc1 - parc2;
		return a;
	}
	
	public static Double multiplicacao(double parc1, double parc2) {
		double a= parc1 * parc2;
		return a;
	}
	
	public static Double divisao(double parc1, double parc2) {
		double a= parc1 / parc2;
		return a;
	}
	
	public static void resultado(int op, double vlr1, double vlr2) {
		
		double saida;
		
		switch (op) {
		case 1:
			saida=soma(vlr1,vlr2);
			System.out.println("O Resultado da soma �: "+ saida);
			break;
		case 2:
			saida=subtracao(vlr1,vlr2);
			System.out.println("O Resultado da subtracao �: "+ saida);
			break;
		case 4:
			saida=multiplicacao(vlr1,vlr2);
			System.out.println("O Resultado da multiplicacao �: "+ saida);
			break;
		case 5:
			saida=divisao(vlr1,vlr2);
			System.out.println("O Resultado da divisao �: "+ saida);
			break;

		default:
			System.out.println("N�o existe essa opera��o dentre as op��es fornecidas");
			break;
		}
		
	}

}
