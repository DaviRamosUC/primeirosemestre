package br.ucsal;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Date;

public class DateAtual {

	public static void executor() {
		Date variavelTemporaria = dataAtual();
		Impressao.imprimir(variavelTemporaria);
		
		String dataFormatada = dataFormatada(variavelTemporaria);
		Impressao.imprimir(dataFormatada);
		
		Impressao.imprimir(dataFormatadaSDF(variavelTemporaria));
		
		Impressao.imprimir("Estamos no ano de: "+anoAtual());
		Impressao.imprimir("A hora atual �: "+horaAtual());
	}
	
	public static String dataFormatadaSDF (Date data) {
		SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");
		return sdf.format(data).toLowerCase();
	}
	
	public static int anoAtual() {
		LocalDate ld = LocalDate.now();
		return ld.getYear();
	}
	
	public static int horaAtual() {
		LocalTime lt = LocalTime.now();
		return lt.getHour();
	}
	
	public static Date dataAtual() {
		Date dataAtual = new Date();
		return dataAtual;
	}
	
	public static String dataFormatada(Date data) {
		return DateFormat.getDateInstance().format(data);
	}

	
	public static void main(String[] args) {
		executor();
	}

}
