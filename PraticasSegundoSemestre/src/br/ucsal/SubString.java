package br.ucsal;

public class SubString {
	
	public static void executar() {
		String txt = "Pedro �lvares Cabral", textoTrabalhado;
		imprimir(txt);
		
		textoTrabalhado = primeiraPalavra(txt);
		imprimir(textoTrabalhado);
		textoTrabalhado = ultimaPalavra(txt);
		imprimir(textoTrabalhado);
		textoTrabalhado = segundaPalavra(txt);
		imprimir(textoTrabalhado);
		textoTrabalhado = primeiraLetraSobrenome(txt);
		imprimir(textoTrabalhado);
	}
	
	public static String primeiraPalavra(String txt) {
		return txt.substring(0,5);
	}
	
	public static String segundaPalavra(String txt) {
		return txt.substring(6,13);
	}
	
	public static String ultimaPalavra(String txt) {
		return txt.substring(0,5);
	}
	
	public static String primeiraLetraSobrenome(String txt) {
		return txt.substring(0,1);
	}
	
	

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	public static void imprimir (String txt) {
		System.out.println("\n" + txt);
	}

}
