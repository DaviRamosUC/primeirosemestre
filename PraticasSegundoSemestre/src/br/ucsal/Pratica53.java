package br.ucsal;

public class Pratica53 {
/* A partir da sequencia formada na pr�tica 52, elabore um m�todo para cada item a seguir:
a) a m�dia aritm�tica de todos os elementos da sequencia
b) a m�dia ponderada do 4� e 7� elementos da sequencia formada, considerando os pesos 4 e 6 respectivamente (esq -> dir, cima -> baixo)
c) a sequencia em ordem decrescente */
	
	public static void main(String[] args) {
		final int L = 3, C = 5;
		final int[][] ARR = obterArrayBidi(L,C);
		imprimir("a) ", mediaA(ARR, L, C));
		imprimir("b) ", mediaP(ARR, 4, 6));
		imprimir("c) ",obterArrayDecrescente(ARR, L, C));
	}
	
	public static int [][] obterArrayBidi(int l,int c) {
		int[][] arr = new int [l][c];
		for (int x = 0, z = 8; x < arr.length; x++) {
			for (int y = 0; y < arr[x].length; y++, z += 2) {
				arr[x][y] = z;
			}
		}
		return arr;
	}
	
	public static double mediaA(int arr[][], int l, int c) {
		double soma = 0;
		for (int x = 0; x < arr.length; x++) {
			for (int y = 0; y < arr[x].length; y++) {
				soma += arr[x][y];
			}
		}
		return soma/(l*c) ;
	}
	
	public static double mediaP(int arr[][], int p1, int p2) {
		double ele4 = arr[0][3];
		double ele7 = arr[1][1];
		double mediap = (ele4*p1 + ele7*p2) / (p1+p2);
		return mediap;
		
	}
	
	public static int[][] obterArrayDecrescente(int arr[][], int l, int c) {
		int arrd [][] = new int [l][c];		
		for (int x = 0, x1 = arr.length - 1; x < arr.length; x++, x1--) {
			for (int y = 0, y1 = arr[x].length - 1; y < arr[x].length; y++, y1--) {
				arrd[x][y] = arr[x1][y1];
			}
		}
		return arrd;		
	}	
	
	public static void imprimir (String txt, int[][] arr) {
		System.out.println(txt);
		for (int x = 0; x < arr.length; x++) {
			for (int y = 0; y < arr[x].length; y++) {
				if(arr[x][y] < 10)
					System.out.print(" ");
				System.out.print(arr[x][y] + " ");
			}
			System.out.println();
		}
	}
	
	public static void imprimir (String txt,double vlr) {
		System.out.print(txt);
		System.out.println(vlr);
		System.out.println();
	}
	
}