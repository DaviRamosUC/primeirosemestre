package br.ucsal;

public class Pratica42 {

	public static void executar() {
		String txt = "Fabricio Queiroz Bezerra da Silva";
		
		imprimir(inversao(txt));

	}

	public static String inversao(String txt) {
		String invertido = " ";
		
		for (int i = txt.length(); i > 0; i--) {
			invertido += txt.substring(i-1, i); 
		}
		
		
		return invertido;
		
	}


	public static void imprimir(String txt) {

		System.out.print(txt);

	}

	public static void main(String[] args) {

		executar();
	}

}
