package br.ucsal;

public class Pratica48 {

	/*
	 * Elabora uma solu��o para construir dinamicamente uma sequencia de valores
	 * inteiros, crescentes, iniciada pelo valor 1, contendo 30 elementos. Considere
	 * que a sequencia dever� ser constru�da de forma que cada valor da sequencia
	 * seja um elemento de um array unidimensional que ser� retornado de um m�todo.
	 * Ao final o array dever� ser impresso apresentando a sequencia elaborada.
	 */

	public static void main(String[] args) {
		Impressao.imprimir(obterSequencia(30));
	}

	private static int[] obterSequencia(int tam) {
		int [] seq = new int[tam];
		
		for (int i = 0; i < seq.length; i++) {
			seq[i]=i+1;
		}
		
		return seq;
	}

}
