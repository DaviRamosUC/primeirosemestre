package br.ucsal;

public class Pratica50 {

	/*
	 * Na matem�tica, a Sucess�o de Fibonacci (ou Sequ�ncia de Fibonacci), � uma
	 * sequ�ncia de n�meros inteiros, come�ando normalmente por 0 e 1, na qual, cada
	 * termo subsequente corresponde � soma dos dois anteriores.
	 * 
	 * Diante da explica��o, elabore uma solu��o para preencher um array
	 * unidimensional com uma sequencia com os 30 primeiros valores inteiros,
	 * positivos de uma sequencia fibonacci. Lembrando que a sequencia inicia com 0
	 * e 1.
	 */
	public static void main(String[] args) {
		Impressao.imprimir(preenchendoOVetor());
	}
	
	// 0 1 1 2 3 5 8 13
	
	public static int [] preenchendoOVetor () {
		int [] vetor = new int [30];
		for (int i = 0; i < vetor.length; i++) {
			if (i==0 || i==1) {
				vetor[i]=i;
			}else {
				vetor [i]=vetor[i-1]+vetor[i-2];
			}			
		}
		return vetor;
	}

}
