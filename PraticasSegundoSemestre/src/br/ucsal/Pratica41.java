package br.ucsal;

public class Pratica41 {

	public static void executar() {
		String txt = "Fabricio Queiroz Bezerra da Silva";
		imprimir(sobrenomeNome(txt) + ", " + nome(txt));

	}

	public static String sobrenomeNome(String txt) {
		int espaco = 0;
		for (int i = 0; i < txt.length(); i++) {
			if (txt.substring(i, i + 1).equals(" ")) {
				espaco = i;
			}
		}
		return txt.substring(espaco + 1, txt.length());
	}

	public static String nome (String txt) {

		int espaco = 0;
		String nome = "Deu errado";

		for (int i = 0; i < txt.length(); i++) {
			if (txt.substring(i, i + 1).equals(" ")) {
				espaco++;
				if (espaco == 1) {
					nome = txt.substring(0, i);
				}
			}
		}
		return nome;
	}

	public static void imprimir(String txt) {

		System.out.print(txt);

	}

	public static void main(String[] args) {

		executar();
	}

}
