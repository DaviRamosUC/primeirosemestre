package br.ucsal;

//import java.text.DateFormat;
//import java.time.LocalDate;
import java.util.Calendar;
//import java.util.Date;

public class Pratica44 {

	/*
	 * Elabore uma solu��o para que a partir da data de nascimento informada pelo
	 * usu�rio, no formato xx/xx/xxxx, seja calculado e retornado a quantidade de
	 * dias que o usu�rio tem de vida.
	 * 
	 * linus benedict torvalds - 28/12/1969
	 */

	public static void executor() {
//		final String nome = "Linus benedict torvalds";
		final String dataNascimento = "28/12/1969";

		// impress�es
		Impressao.imprimir(primeiroAnoVida(dataNascimento));
		Impressao.imprimir(anoAtualVida());
		Impressao.imprimir(qtdeDiasVida(dataNascimento));
	}
	
	public static int qtdeDiasVida(String dataNascimento) {
		int qtddeDias = primeiroAnoVida(dataNascimento) + anoAtualVida() + anosIntermediariosVida(dataNascimento);
		return qtddeDias;
	}

	public static int primeiroAnoVida(String dataNascimento) {
		int qtdDias = 0;
		int dia = Integer.parseInt(dataNascimento.substring(0, 2));
		int mes = Integer.parseInt(dataNascimento.substring(3, 5));
		int ano = Integer.parseInt(dataNascimento.substring(6, 10));

		if (mes < 12) {
			for (int i = mes + 1; i <= 12; i++) {
				qtdDias += diasDoMes(ano, i);
			}
		}
		qtdDias += diasDoMes(ano, mes) - dia;
		qtdDias += 1;

		return qtdDias;
	}

	public static int anosIntermediariosVida(String dataNasc) {
		Calendar calendar = Calendar.getInstance();
		int qtdeDias = 0, 
			anoPosterior = Integer.parseInt(dataNasc.substring(6, 10)) + 1,
			anoAnterior = calendar.get(Calendar.YEAR) - 1;
		qtdeDias = (anoAnterior - anoPosterior + 1) * 365;
		for (int i = anoPosterior; i <= anoAnterior; i++) 
			if ((anoPosterior % 4 == 0 && anoPosterior % 100 != 0) || anoPosterior % 400 == 0)
				qtdeDias++;
		return qtdeDias;
	}

	public static int anoAtualVida() {
		Calendar calendar = Calendar.getInstance();
		int 	qtdias = 0;
		calendar.get(Calendar.DATE);
		int mes = calendar.get(Calendar.MONTH), ano = calendar.get(Calendar.YEAR);

		for (int i = 1; i < mes; i++) {
			qtdias += diasDoMes(ano, i);
		}
		qtdias += diasDoMes(ano, mes);
		return qtdias;
	}

	public static int diasDoMes(int ano, int mes) {
		int qtdDias = 0;
		switch (mes) {
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
			qtdDias = 31;
			break;
		case 4:
		case 6:
		case 9:
		case 11:
			qtdDias = 30;
			break;
		case 2:
			if ((ano % 4 == 0 && ano % 100 != 0) || ano % 400 == 0) {
				qtdDias = 29;
			} else
				qtdDias = 28;
			break;
		default:
			qtdDias = 0;
			break;
		}
		return qtdDias;
	}

	public static void main(String[] args) {
		executor();
	}
}
