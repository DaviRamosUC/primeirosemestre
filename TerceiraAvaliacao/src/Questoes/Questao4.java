package Questoes;

import java.util.Scanner;

public class Questao4 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		
		int num1 = 1, num2 = 0, vlrs=0, divseis=0, divtreis=0, penu=0;
		int i = 1;
		
		System.out.print("A sequencia �: ");
		do {
			//Letra A
			num1 = num1 + num2;
			num2 = num1 - num2;
			System.out.print(" "+num1);
			//Letra B
			vlrs = vlrs+ num1;
			
			//Letra C
			if ((num1 % 6 ==0 || num2 % 6 ==0) && (num1 % 2 ==0 || num2 % 2 ==0)) {
				divseis +=1;
			}
			
			//Letra D
			if ((num1 % 2 !=0 || num2 % 2 !=0) && (num1 % 3 ==0 || num2 % 3 ==0)) {
				divtreis +=1;
			}
			
			//Letra E
			if (i==28) {
				penu=num1;
			}
			
			i++;
		} while (i <= 30);
		
		System.out.println("\nA soma de todos os valores da sequ�ncia �: "+vlrs);
		System.out.println("A quantidade de n�meros pares divisiveis por 6 s�o: "+divseis);
		System.out.println("A quantidade de n�meros �mpares divisiveis por 3 s�o: "+divtreis);
		System.out.println("O pen�ltimo valor da sequencia �: "+penu);
		
		sc.close();
		
		
	}

}