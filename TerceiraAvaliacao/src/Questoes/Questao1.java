package Questoes;

import java.util.Scanner;

public class Questao1 {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		int i =0;
		final String dia1="Domingo",dia2="Segunda",dia3="Ter�a",dia4="Quarta",dia5="Quinta",dia6="Sexta",dia7="S�bado";
		
		do {
			
			System.out.println("Informe um valor de 1 � 7");
			int vlr = sc.nextInt();
			
			if(vlr<1 || vlr>7) {
				System.out.println("Valor informado est� fora do limite");
			}else {
				switch (vlr) {
				case 1:
					System.out.println("O dia correspondente ao n�mero informado �: "+dia1);
					break;
				case 2:
					System.out.println("O dia correspondente ao n�mero informado �: "+dia2);
					break;
				case 3:
					System.out.println("O dia correspondente ao n�mero informado �: "+dia3);
					break;
				case 4:
					System.out.println("O dia correspondente ao n�mero informado �: "+dia4);
					break;
				case 5:
					System.out.println("O dia correspondente ao n�mero informado �: "+dia5);
					break;
				case 6:
					System.out.println("O dia correspondente ao n�mero informado �: "+dia6);
					break;
				case 7:
					System.out.println("O dia correspondente ao n�mero informado �: "+dia7);
					break;



				default:
					break;
				}
			}
			
			i++;
		} while (i<5);
		
		sc.close();

	}

}
