package Questoes;

import java.util.Scanner;

public class Questao2 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		final int limite = 675, erro = 3;

		int vlri = 0;
		int numerro = 1, controle=0;
		System.out.println("Informe o valor inicial");
		vlri = sc.nextInt();
		int vlrf = vlri;
		while (numerro < erro) {

			if (vlri < 40 || vlri > 70) {
				numerro++;
				System.out.println("Valor apresentado fora do intervalo, tente novamente! \nErro cometido: " + (numerro-1));
				vlri = sc.nextInt();
				if (numerro == 3) {
					controle++;
				}
			} else {
				System.out.println("N�mero informado corretamente!");
				while (vlrf<limite) {
					System.out.print(" "+ vlrf);
					vlrf +=5;					
				}
				
				numerro = erro;
			}			

		}

		if (controle==1) {
			System.out.println("Valor apresentado fora do intervalo! \nErro cometido: " + (numerro));
			System.out.println("Limite de erros atingidos fim da execu��o");
		}
		sc.close();

	}

}
