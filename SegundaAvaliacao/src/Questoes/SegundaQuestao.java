package Questoes;

import java.util.Scanner;

public class SegundaQuestao {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		double imc = 0, peso, altura;
		int idade, codigo = 0;
		String situacao = "a";
		final String st1 = "Abaixo do peso", st2 = "Peso Normal", st3 = "Sobrepeso", st4 = "Obesidade I",
				st5 = "Obesidade III";

		System.out.println("Informe a sua idade");
		idade = sc.nextInt();
		//O senhor n�o solicitou controle de idade por�m fazia parte do enunciado
		if (idade >= 18) {
			System.out.println("Informe seu peso");
			peso = sc.nextDouble();
			System.out.println("Informe sua altura");
			altura = sc.nextDouble();

			imc = (peso / (altura * altura));

			if (imc < 18.5) {
				codigo = 1;
			} else if (imc >= 18.5 && imc <= 24.99) {
				codigo = 2;
			} else if (imc >= 25.00 && imc <= 29.99) {
				codigo = 3;
			} else if (imc >= 30.00 && imc <= 39.99) {
				codigo = 4;
			} else if (imc >= 40.00) {
				codigo = 5;
			}

			switch (codigo) {
			case 1:
				situacao = st1;
				break;
			case 2:
				situacao = st2;
				break;
			case 3:
				situacao = st3;
				break;
			case 4:
				situacao = st4;
				break;
			case 5:
				situacao = st5;
				break;
			default:
				System.out.println("Voc� n�o se encaixa em nenhuma das faixas");
				break;
			}
		} else {
			System.out.println("Voc� se encontra abaixo dos 18 anos");
		}

		if (situacao != "a") {
			System.out.println("Voc� se encaixa na faixa de c�digo: " + codigo + " \nSua situa��o �: " + situacao+ " \nVoc� dever� praticar atividades fisicas por "+ codigo+" vezes/semana");
			//System.out.println(imc);
		}
		sc.close();

	}

}
