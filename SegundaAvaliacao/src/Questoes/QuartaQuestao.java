package Questoes;

import java.util.Scanner;

public class QuartaQuestao {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		final double aliquota1 = 0, aliquota2 = (7.5 / 100), aliquota3 = (15.0 / 100), aliquota4 = (22.5 / 100),
				aliquota5 = (27.5 / 100);
		double aliquota = 0, desconto = 0;
		final double desconto1 = (0.00), desconto2 = (142.80), desconto3 = (354.80), desconto4 = (636.13),
				desconto5 = (869.36);
		double resultadopc = 1, resultado = 0, salarioLiquido=1;

		System.out.println("Informe seu Sal�rio Bruto");
		Double salarioBruto = sc.nextDouble();

		if (salarioBruto > 0) {
			if (salarioBruto <= (1903.98)) {
				resultadopc = (salarioBruto);
				resultado = (resultadopc);
				aliquota = aliquota1;
				desconto = desconto1;
			} else if ((salarioBruto >= 1903.99) && (salarioBruto <= 2826.65)) {
				resultadopc = (salarioBruto * aliquota2);
				resultado = (resultadopc - desconto2);
				aliquota = aliquota2;
				desconto = desconto2;
			} else if ((salarioBruto >= 2826.66) && (salarioBruto <= 3751.05)) {
				System.out.println(salarioBruto+" "+aliquota3);
				resultadopc = (salarioBruto * aliquota3);
				System.out.println(resultadopc);
				resultado = (resultadopc - desconto3);
				System.out.println(resultado);
				aliquota = aliquota3;
				desconto = desconto3;
			} else if ((salarioBruto >= 3751.06) && (salarioBruto <= 4664.68)) {
				resultadopc = (salarioBruto * aliquota4);
				resultado = (resultadopc - desconto4);
				aliquota = aliquota4;
				desconto = desconto4;
			} else if (salarioBruto > 4664.68) {
				resultadopc = (salarioBruto * aliquota5);
				resultado = (resultadopc - desconto5);
				aliquota = aliquota5;
				desconto = desconto5;
			}

		} else {
			System.out.println("Valor de salario bruto � negativo");
		}

		if (salarioBruto <= (1903.98)) {
			salarioLiquido = (salarioBruto);
			System.out.println("Seu Salario Bruto � de: " + salarioBruto + "\nA sua aliquota � de: " + aliquota
					+ "\nO seu desconto � de: " + desconto + "\nO seu Salario Liquido � de " + salarioLiquido);
		} else {
			salarioLiquido = (salarioBruto - resultado);
			System.out.println("Seu Salario Bruto � de: " + salarioBruto + "\nA sua aliquota � de: " + aliquota
					+ "\nO seu desconto � de: " + desconto + "\nO seu Salario Liquido � de " + salarioLiquido);
		}

		sc.close();
	}

}
