package Questoes;

import java.util.Scanner;

public class TerceiraQuestao {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		final double p1 = 2.5, p2 = 3.0, p3 = 4.5;
		double v1, v2, v3, soma = 0, media = 0;
		final String ap = "Aprovado", rp = "Reprovado";

		System.out.println("Informe a primeira nota");
		v1 = sc.nextInt();
		System.out.println("Informe a segunda nota");
		v2 = sc.nextInt();
		System.out.println("Informe a terceira nota");
		v3 = sc.nextInt();

		soma = p1 + p2 + p3;
		if ((v1 >= 0 && v1 <= 10) && (v2 >= 0 && v2 <= 10) && (v3 >= 0 && v3 <= 10)) {

			media = ((v1 * p1) + (v2 * p2) + (v3 * p3)) / soma;

			if (media >= 6) {
				System.out.println("O aluno foi " + ap + "\nCom media: " + media);

			} else if (media < 6) {
				System.out.println("O aluno foi " + rp + "\nCom media: " + media);
			}

		} else {
			System.out.println("Valor fora do intervalo.");
		}
		sc.close();

	}

}
