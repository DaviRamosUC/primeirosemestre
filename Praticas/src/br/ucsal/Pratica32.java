package br.ucsal;

import java.util.Scanner;

public class Pratica32 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		int num1 = 0, num2 = 1, i = 0;

		do {
			System.out.print(num1 + " ");
			num1 = num1 + num2;
			num2 = num1 - num2;
			i++;

		} while (i < 20);
		sc.close();
	}

}
