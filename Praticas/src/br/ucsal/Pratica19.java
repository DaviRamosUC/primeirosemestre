package br.ucsal;

import java.util.Scanner;

public class Pratica19 {

	/*
	 * Elabore uma solu��o utilizando switch/case para, a partir de um valor
	 * informado no intervalo de 1 a 7, seja impresso o dia da semana por extenso.
	 * Valores informados fora desse intervalo imprimir "dia inv�lido"
	 */
	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		int a = 0;
		a = sc.nextInt();

		System.out.println("Digite um valor de 1 a 7");
		if (a >= 1 && a <= 7) {
			switch (a) {
			case 1:
				System.out.println("Domingo");
				break;
			case 2:
				System.out.println("Segunda");
				break;
			case 3:
				System.out.println("Ter�a");
				break;
			case 4:
				System.out.println("Quarta");
				break;
			case 5:
				System.out.println("Quinta");
				break;
			case 6:
				System.out.println("Sexta");
				break;
			case 7:
				System.out.println("S�bado");
				break;
			default:
				System.out.println("O valor digitado n�o corresponde a um dia da semana");
				break;
			}
		}
		sc.close();
	}

}
