package br.ucsal;

import java.util.Scanner;

public class Pratica25 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		double vlr1, vlr2, resultado = 0;

		System.out.println("Informe qual s�o os valores que voc� deseja fazer a opera��o");
		vlr1 = sc.nextDouble();
		vlr2 = sc.nextDouble();
		if ((vlr1 >= 1) && (vlr2 >= 1)) {
			System.out.println(
					"Informe qual das oper��es voc� deseja fazer \n(1) Soma \n(2) Subtra��o \n(3) Multiplica��o \n(4) Divis�o");
			int x = sc.nextInt();
			switch (x) {
			case 1:
				resultado = (vlr1 + vlr2);
				break;
			case 2:
				resultado = (vlr1 - vlr2);
				break;
			case 3:
				resultado = (vlr1 * vlr2);
				break;
			case 4:
				resultado = (vlr1 / vlr2);
				break;
			default:
				System.out.println("Valor inv�lido");
				break;
			}
		} else {
			System.out.println("N�mero informado abaixo de 1");
		}
		System.out.println("O resultado �: " + resultado);
		sc.close();

	}

}
