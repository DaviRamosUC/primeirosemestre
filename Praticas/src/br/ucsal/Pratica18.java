package br.ucsal;

import java.util.Scanner;

public class Pratica18 {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		double a, b;
		final double d=4, e= 6, soma= d+e;
		final String AP = "Aprovado", RP = "Reprovado";
		
		System.out.println("Digite tr�s notas");
		a=sc.nextDouble();
		b=sc.nextDouble();
		
		double resultado = (((a*d)+(b*e))/soma);
		
		if (resultado>=6) {
			System.out.println(AP);
		}else {
			System.out.println(RP);
		}
		sc.close();

	}

}
