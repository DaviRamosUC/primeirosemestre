package br.ucsal;

import java.util.Scanner;

public class pratica14 {

	public static void main(String[] args) {
		/* Elabore uma solu��o em JAVA para ler o nome e o seu endere�o completo 
		 * (separados por tipo: rua, numero, bairro, cep, cidade,estado) informados pelo usu�rio. 
		 * A solu��o deve retornar para o usu�rio uma impress�o de sa�da contendo o nome e o endere�o completo informado.
		 * Para essa solu��o, cada elemento informado deve ser inserido em uma vari�vel espec�fica com o t�tulo CADASTRO.
		 * Obs: use o n� com o tipo int*/
		
		Scanner sc = new Scanner (System.in);
		String nome,rua,cep,bairro,cidade,estado;
		int numero;
		
		System.out.println("Informe seu nome");
		nome=sc.next();
		
		System.out.print("Informe seu endere�o completo \nInforme o CEP: ");
		cep=sc.next();
		System.out.println("Informe o seu estado: ");
		estado = sc.next();
		System.out.print("Informe a cidade: ");
		cidade = sc.next();
		System.out.print("Informe o bairro: ");
		bairro= sc.next();
		System.out.print("Informe o nome da rua: ");
		rua=sc.next();
		System.out.print("Informe o n�mero da casa: ");
		numero=sc.nextInt();
		
		System.out.println("CADASTRO");
		System.out.println("Ol� "+nome+", seu endere�o �: \nCEP:"+cep+"\nEstado: "+estado+"\nCidade: "+cidade+"\nBairro: "+bairro+"\nRua: "+rua+" - "+numero);
		
		sc.close();
	}

}
