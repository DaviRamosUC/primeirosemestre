package br.ucsal;

import java.util.Scanner;

public class Pratica34 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		int a = 10, vlrLimite, i = 0, b = 0, c = 0, d = 0, varr = 1, res = 0;
		double soma = 0, media = 0;

		do {

			System.out.println("Informe o Valor limite da sequencia!");
			vlrLimite = sc.nextInt();
			if (vlrLimite < 50 || vlrLimite > 100) {
				System.out.println("Tente novamente!");
			} else {
				i++;
			}

		} while (i == 0);

		System.out.print("A) A sequencia �: ");
		i = 0;
		int vlr10 = 0, vlr4 = 0;
		do {
			System.out.print(+a + " ");
			soma = soma + a;
			a += 6;
			i++;
			varr++;
			if (a % 2 == 0) {
				b++;
			} else {
				c++;
			}
			if (a % 3 == 0) {
				d++;
			}
			if (varr == 4) {
				vlr4 = a;
			}
			if (varr == 10) {
				vlr10 = a;
			}
		} while (a <= vlrLimite);

		res = (10 + a - 6) / 2;
		System.out.println(" ");
		System.out.println("B) O n�mero de elementos na sequ�ncia �: " + i);
		System.out.println("C) A m�dia aritimedica do primeiro e �ltimo termo �: " + res);
		if (b != 0) {
			System.out.println("D) A quantidade de n�meros pares �: " + b);
		}
		System.out.println("E) A quantidade de n�meros impares �: " + c);
		System.out.println("F) A quantidade de n�meros divisiveis por 3 �: " + d);

		System.out.println("G) A soma de todos os termos �: " + soma);
		if (vlr10 != 0) {
			media = (((vlr4 * 4.0) + (vlr10 * 6.0)) / 10);
			System.out.println("H) A m�dia ponderada do 4� e 10� termo �: " + media);
		} else {
			System.out.println("N�o existe valor na 10� posi��o");
		}

		sc.close();
	}

}
