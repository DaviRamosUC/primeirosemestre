package br.ucsal;

import java.util.Scanner;

public class Pratica22 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		System.out.println(
				"Escolha dentre as op��es abaixo, um animal considerado invertebrado: \n� Lesma \n� Molusculo \n� Cachorro \n� Gato");
		String animal = sc.next();
		int x = 0;

		switch (animal) {
		case "Lesma":
		case "Moluscolo":
			x = 1;
			break;

		case "Cachorro":
		case "Gato":
			x = 0;
			break;

		default:
			System.out.println("Voc� escreveu o nome do animal errado");
			break;
		}

		if (x == 1) {
			System.out.println("\nVerdadeiro");
		} else {
			System.out.println("\nFalso");
		}

		sc.close();
	}

}
