package br.ucsal;

import java.util.Scanner;

public class Pratica33 {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner (System.in);
		
		int a=0;
		
		do {
			
			if (a%2==0) {
				if (a%5==0) {
					System.out.print(a+" ");
				}
			}
			a+=1;
			
		} while (a<=100);
		sc.close();
	}

}
