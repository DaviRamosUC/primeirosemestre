package br.ucsal;

import java.util.Scanner;

public class Pratica24 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		double a, b, c;
		double maior = 0, menor = 0;

		System.out.println("Informe tr�s valores: ");
		a = sc.nextDouble();
		b = sc.nextDouble();
		c = sc.nextDouble();

		if ((a >= 10 && a <= 90) && (b >= 10 && b <= 90) && (c >= 10 && c <= 90)) {
			if ((a == b) || (b == c) || (c==a)) {
				System.out.println("N�meros inv�lidos");
			} else {
				if ((a > b) && (b > c)) {
					maior = a;
					menor = c;
				} else if ((a > b) && (c > b)) {
					maior = a;
					menor = b;
				}
				if ((b > a) && (a > c)) {
					maior = b;
					menor = c;
				} else if ((b > a) && (c > a)) {
					maior = b;
					menor = a;
				}
				if ((c > a) && (a > b)) {
					maior = c;
					menor = b;
				} else if ((c > a) && (b > a)) {
					maior = c;
					menor = a;
				}
			}

		} else {
			System.out.println("Os n�meros informados n�o pertencem ao intervalo de 10 � 90");
		}
		if ((maior != 0) && (menor != 0)) {
			System.out.println("O maior n�mero �: " + maior + "\nO menor n�mero �: " + menor);
			sc.close();
		}
	}
}
