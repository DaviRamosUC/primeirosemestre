package br.ucsal;

import java.util.Scanner;

public class Pratica16 {

	/*
	 * Elabore uma solu��o em JAVA no qual, a partir de 03 valores informados em um
	 * intervalo fechado de 10 a 30 do conjunto dos n�meros inteiros, seja mostrado
	 * se os valores correspondem a um tri�ngulo (condi��o de exist�ncia do
	 * tri�ngulo). Se negativo, a informa��o retornada ao usu�rio dever� ser
	 * "valores n�o representam um tri�ngulo" e encerra o algoritmo. Se
	 * positivo,deve-se informar qual a classifica��o desse tri�ngulo quanto aos
	 * lados: equil�tero, escaleno ou is�sceles e encerra o algoritmo. Se os valores
	 * n�o estivem no intervalo, deve-se solicitar que sejam informados novamente.
	 */

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		int a, b, c;

		System.out.println("Informe tr�s valores");
		a = sc.nextInt();
		b = sc.nextInt();
		c = sc.nextInt();

		if ((a >= 10 && 30 >= a) && (b >= 10 && 30 >= b) && (c >= 10 && 30 >= c)) {
			if (a < b + c) {
				if (b < a + c) {
					if (c < a + b) {
						if (a == b && b == c) {
							System.out.println("Triangulo equil�tero");
						} else if (a != b && a != c && c != b) {
							System.out.println("Triangulo escaleno");
						} else {
							System.out.println("Triangulo is�sceles");
						}
					} else {
						System.out.println("valores n�o representam um tri�ngulo");
					}
				}
			}
		} else {
			System.out.println("Favor informar os tr�s valores novamente");
		}
		sc.close();
	}

}
