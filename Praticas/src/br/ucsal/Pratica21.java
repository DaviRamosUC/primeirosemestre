package br.ucsal;

import java.util.Scanner;

public class Pratica21 {
	
	/*Elabore uma solu��o para, a partir da idade informada pelo usu�rio, identificar a categoria do candidato a jogador de acordo com a tabela abaixo:
		=========================================
		Identificador      Faixa Et�ria                     Categoria
		=========================================
		(1)                5 a 7 anos              	Infantil A
		(2)                8 a 10 anos              Infantil B
		(3)                11 a 13 anos             Juvenil C
		(4)                14 a 17 anos             Juvenil D
		(5)                A partir dos 18 anos     Adulto
*/

	public static void main(String[] args) {
		 
		Scanner sc = new Scanner (System.in);
		
		final String cat1 = "Infantil A", cat2 = "Infantil B", cat3 = "Juvenil C", cat4 = "Juvenil D", cat5 = "Adulto";
		int i=0;
		
		System.out.println("Informe a idade do candidato");
		int idade= sc.nextInt();
		
		if (idade>=5 && idade <=7) {
			i=1;
		}else if(idade>=8 && idade<=10) {
			i=2;
		}else if(idade>=11 && idade<=13) {
			i=3;
		}else if(idade>=14 && idade<=17) {
			i=4;
		}else if(idade>=18) {
			i=5;
		}
		
		switch (i) {
		case 1:
			System.out.println("A categoria do candidato com a idade "+idade+" �: "+cat1);
			break;
		case 2:
			System.out.println("A categoria do candidato com a idade "+idade+" �: "+cat2);
			break;
		case 3:
			System.out.println("A categoria do candidato com a idade "+idade+" �: "+cat3);
			break;
		case 4:
			System.out.println("A categoria do candidato com a idade "+idade+" �: "+cat4);
			break;
		case 5:
			System.out.println("A categoria do candidato com a idade "+idade+" �: "+cat5);
			break;
		default:
			System.out.println("O candidato n�o pode participar do jogo");
			break;
		}
		
		sc.close();

	}

}
