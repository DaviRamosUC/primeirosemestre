package br.ucsal;

import java.util.Scanner;

public class Pratica36 {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		int num = 0;
		for (int i = 0; (num < 50 || num > 60) && i < 3; i ++) {
			System.out.println("Digite o primeiro termo (50 a 60)");
			num = sc.nextInt();
		}
		if (num < 50 || num > 60) {
			System.out.println("Limite de erros atingidos - fim da execu��o");
		} else {
			for (int i = 0; i < 10; i++, num*=5) {
				System.out.print(num + " ");
			}
		}
		sc.close();

	}

}
