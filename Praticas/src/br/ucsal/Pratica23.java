package br.ucsal;

import java.util.Scanner;

public class Pratica23 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.println("Informe o ano e o m�s que deseja consultar");
		int ano = sc.nextInt();
		int mes = sc.nextInt();

		int teste1 = (ano % 4),
			teste2 = (ano % 100),
			teste3 = (ano % 400);
		int fev = 0;

		if (teste1 == 0) {
			if (teste2 == 0) {
				System.out.println("O ano n�o � bissexto");
				fev = 28;
			} else {
				System.out.println("O ano � bissexto");
				fev = 29;
			}
		} else {
			if (teste3 == 0) {
				System.out.println("O ano � bissexto");
				fev = 29;
			} else {
				System.out.println("O ano n�o � bissexto");
				fev = 28;
			}
		}

		switch (mes) {
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
			System.out.println("O m�s escolhido tem 31 dias");

			break;
		case 2:
			System.out.println("O m�s escolhido tem "+fev+" dias");
			break;
		case 4:
		case 6:
		case 9:
		case 11:
			System.out.println("O m�s escolhido tem 30 dias");
			break;
		default:
			System.out.println("M�s maior que 12 ou menor que 1 informado");
			break;
		}
		
		sc.close();

	}

}
