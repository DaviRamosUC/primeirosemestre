package br.ucsal;

import java.util.Scanner;

public class Pratica20 {

	/*
	 * A partir da velocidade obtida do ve�culo do usu�rio, registre a multa aplicada considerando a seguinte tabela:
		acima de  40Km ................. R$ 160,00
		acima de  60Km ................. R$ 200,00
		acima de  80Km ................. R$ 300,00 
	*/
	public static void main(String[] args) {
		
		Scanner sc = new Scanner (System.in);
		
		System.out.println("Insira a velocidade");
		
		final double vlr1=160, vlr2=200, vlr3=300;
		
		double x = sc.nextDouble();
		
		if (x>=40 && x<=200) {
			System.out.printf("Valor da multa R$ %s",vlr1);
			
		}else if (x>=200 && x<=300) {
			System.out.printf("Valor da multa R$ %s",vlr2);
		}else {
			System.out.printf("Valor da multa R$ %s",vlr3);
		}
		sc.close();
	}

}
