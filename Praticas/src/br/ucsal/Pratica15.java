package br.ucsal;

import java.util.Scanner;

public class Pratica15 {

	/*
	 * Elabore uma solu��o em JAVA para o usu�rio informar o m�s e um dia qualquer.
	 * No valor do dia informado deve-se aplicar uma soma de mais 10 dias. 
	 * Se o resultado dessa soma for igual ou menor que 30 deve-se apenas imprimir o m�s
	 * e o dia resultante, caso contr�rio o m�s impresso deve ser o que vem ap�s o
	 * m�s informado e o dia resultante ser� o dia somado mais 10 menos 30. Exemplo:
	 * a) O usu�rio informa m�s 8 e dia 10 -> a impress�o final ser�:
	 * "m�s 8 e dia 20" b) O usu�rio informa m�s 9 e dia 25 -> a impress�o final
	 * ser�: "m�s 10 e dia 5" OBS: considere 30 como uma constante na solu��o do
	 * problema.
	 */
	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.println("Informe um dia");
		int dia = sc.nextInt();
		System.out.println("Informe um m�s");
		int mes = sc.nextInt();
		
		if (dia>=30) {
			dia =((dia+10)-30);
			mes++;
		}else {
			dia = dia +10;
		}
		System.out.println("O dia �: "+dia+ "\nO m�s �: "+mes);
		sc.close();
		
	}

}
