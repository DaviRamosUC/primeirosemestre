package br.ucsal;

import java.util.Scanner;

public class Pratica17 {

	/*
	 * Elabore um algoritmo com JAVA para que, a partir de 03 valores
	 * informados,seja calculada a m�dia aritm�tica desses valores. Em seguida deve
	 * ser apresentado o resultado considerando o seguinte crit�rio: se o valor
	 * obtido da m�dia for menor que 6 a informa��o final dever� ser "Reprovado",
	 * caso contr�rio dever� ser "Aprovado".
	 */
	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		double a, b, c;
		final String AP = "Aprovado", RP = "Reprovado";
		
		System.out.println("Digite tr�s notas");
		a=sc.nextDouble();
		b=sc.nextDouble();
		c=sc.nextDouble();
		
		double resultado = ((a+b+c)/3);
		
		if (resultado>=6) {
			System.out.println(AP);
		}else {
			System.out.println(RP);
		}
		sc.close();
	}
}
