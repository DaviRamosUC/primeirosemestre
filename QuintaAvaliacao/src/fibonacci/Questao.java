package fibonacci;

public class Questao {

	public static void main(String[] args) {
		imprimir(obterSequenciaFibonacci(4, 5, 13, 21));
		System.out.println();
		imprimir(obterMediaAritmetica(obterSequenciaFibonacci(4, 5, 13, 21)));
		System.out.println();
		imprimir(obterSequenciaDecrescente(obterSequenciaFibonacci(4, 5, 13, 21)));
		System.out.println();
		imprimir(obterProgressaoAritimetica(obterSequenciaFibonacci(4, 5, 13, 21), 5, 3));
		System.out.println();
		imprimir(obterQuantidadeImpares(obterProgressaoAritimetica(obterSequenciaFibonacci(4, 5, 13, 21), 5, 3)));

	}

	public static int[][] obterSequenciaFibonacci(int l, int c, int z, int x) {
		int[][] matriz = new int[l][c];
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz[i].length; j++) {
				if (i == 0 && j == 0) {
					matriz[i][j] = z;
				} else if (i == 0 && j == 1) {
					matriz[i][j] = x;
				}
				if (j >= 2) {
					matriz[i][j] = matriz[i][j - 1] + matriz[i][j - 2];
				}
				if (i > 0 && j == 0) {
					matriz[i][j] = matriz[i - 1][matriz[i].length - 1] + matriz[i - 1][matriz[i].length - 2];
				}
				if (i > 0 && j == 1) {
					matriz[i][j] = matriz[i][j - 1] + matriz[i - 1][matriz[i].length - 1];
				}

			}
		}

		return matriz;
	}

	public static double obterMediaAritmetica(int arr[][]) {
		double soma = 0, tamX = 0, tamY = 0;
		for (int x = 0; x < arr.length; x++) {
			for (int y = 0; y < arr[x].length; y++) {
				soma += arr[x][y];

			}
			if (x == arr.length - 1) {
				tamX = arr.length;
				tamY = arr[x].length;
			}
		}
		return soma / (tamX * tamY);
	}

	public static int[][] obterSequenciaDecrescente(int arr[][]) {
		int z = 0;
		for (int x = 0; x < arr.length; x++) {
			for (int y = 0; y < arr[x].length; y++) {
			}
			if (x == arr.length - 1) {
				z = arr[x].length;
			}
		}

		int arrd[][] = new int[arr.length][z];
		for (int x = 0, x1 = arr.length - 1; x < arr.length; x++, x1--) {
			for (int y = 0, y1 = arr[x].length - 1; y < arr[x].length; y++, y1--) {
				arrd[x][y] = arr[x1][y1];
			}
		}
		return arrd;
	}

	public static int[][] obterProgressaoAritimetica(int arr[][], int inicio, int razao) {
		int z = 0;
		for (int x = 0; x < arr.length; x++) {
			for (int y = 0; y < arr[x].length; y++) {
			}
			if (x == arr.length - 1) {
				z = arr[x].length;
			}
		}
//		-----------------------------------------------------------------------------------

		int arrd[][] = new int[arr.length][z];
		for (int x = 0; x < arr.length; x++) {
			for (int y = 0; y < arr[x].length; y++) {
				if (x == 0 && y == 0) {
					arrd[x][y] = inicio;
				} else if (x >= 0 && y >= 1) {
					arrd[x][y] = arrd[x][y - 1] + razao;
				}

				if (x > 0 && y == 0) {
					arrd[x][y] = arrd[x - 1][arrd[x].length - 1] + razao;
				}

			}
		}
		return arrd;
	}

	public static int obterQuantidadeImpares(int arr[][]) {
		int qtd = 0;
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {
				if (arr[i][j] % 2 != 0) {
					qtd++;
				}
			}
		}

		return qtd;
	}

//		-----------------------------------------------------------------------------------

	public static void imprimir(int[][] matriz) {
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz[i].length; j++) {
				if (matriz[i][j] < 10) {
					System.out.print(" ");
				}
				System.out.print(matriz[i][j] + " ");
			}
			System.out.println();
		}
	}

	public static void imprimir(double resultado) {
		System.out.println(resultado);
	}

	public static void imprimir(int resultado) {
		System.out.println(resultado);
	}

}
