package fibonacci;

public class Samila {
	public static void executor() {
		final int linha = 4, coluna = 5;
		final int[][] tm = obterSequenciaFibonacci(linha, coluna);
		System.out.println("Item A");
		imprimir(obterSequenciaFibonacci(4, 5));
		System.out.println("");
		System.out.println("");
		System.out.println("Item B");
		imprimir(obterMediaAritmetica(tm, linha, coluna));
		System.out.println("");
		System.out.println("Item C");
		imprimir(obterSequenciaDecrescente(tm, linha, coluna));
		System.out.println("");
		System.out.println("");
		System.out.println("Item D");
		imprimir(obterProgressaoAritmetica(linha, coluna));
		System.out.println("");
		System.out.println("");
		System.out.println("Item E");
		imprimir(obterQuantidadeImpares(tm, linha, coluna));
		;

	}

	public static int[][] obterSequenciaFibonacci(int l, int c) {
		int[][] arr = new int[l][c];
		for (int a = 0, pri = 13, seg = 21, aux; a < arr.length; a++) {
			for (int b = 0; b < arr[a].length; b++) {
				arr[a][b] = pri;
				aux = pri + seg;
				pri = seg;
				seg = aux;
			}
		}
		return arr;
	}

	public static double obterMediaAritmetica(int tam[][], int linha, int coluna) {
		double soma = 0;
		for (int i = 0; i < tam.length; i++) {
			for (int j = 0; j < tam[i].length; j++) {
				soma += tam[i][j];
			}
		}
		return soma / (linha * coluna);
	}

	public static int[][] obterSequenciaDecrescente(int tam[][], int linha, int coluna) {
		int arrd[][] = new int[linha][coluna];
		for (int i = 0, i1 = tam.length - 1; i < tam.length; i++, i1--) {
			for (int j = 0, j1 = tam[i].length - 1; j < tam[i].length; j++, j1--) {
				arrd[i][j] = tam[i1][j1];
			}
		}
		return arrd;
	}

	public static int[][] obterProgressaoAritmetica(int l, int c) {
		int seq[][] = new int[l][c];
		for (int il = 0, p = 5; il < seq.length; il++) {
			for (int ic = 0; ic < seq[il].length; ic++, p += 3) {
				seq[il][ic] = p;
			}
		}
		return seq;
	}

	public static int obterQuantidadeImpares(int tam[][], int l, int c) {
		int timpar = 0;

		for (int il = 0, p = 5; il < tam.length; il++) {
			for (int ic = 0; ic < tam[il].length; ic++, p += 3) {
				tam[il][ic] = p;

				if (tam[il][ic] % 2 == 1) {
					timpar++;
				}
			}

		}
		return timpar;
	}

	// m�todos de impress�o
	public static void imprimir(int[][] vlr) {
		for (int i = 0; i < vlr.length; i++) {
			for (int j = 0; j < vlr[i].length; j++) {
				if (vlr[i][j] < 10) {
					System.out.print("");
				}
				System.out.print(vlr[i][j] + " ");
			}
			System.out.print("");
		}
	}

	public static void imprimir(double vlr) {
		System.out.println(vlr);
	}

	public static void imprimir(String msg) {
		System.out.println("");
	}

	public static void imprimir(int vlr) {
		System.out.println(vlr);
	}

	public static void main(String[] args) {
		executor();

	}

}
